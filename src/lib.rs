// Inspired by https://github.com/chooxide/tachyons/blob/master/src/lib.rs

/// Import `bulma.css` framework
pub const BULMA: &'static str =
  include_str!("../vendor/bulma/css/bulma.css");
