mod node;
mod repo;
mod lib;

use crate::lib::BULMA;

#[macro_use]
extern crate lazy_static;

extern crate clap;

use clap::{App, Arg};
use std::sync::{Mutex, Arc};

#[macro_use] extern crate nickel;
use nickel::mimes::MediaType;

#[macro_use] extern crate horrorshow;

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
                        .version(env!("CARGO_PKG_VERSION"))
                        .author(env!("CARGO_PKG_AUTHORS"))
                        .about(env!("CARGO_PKG_DESCRIPTION"))
                        .arg(Arg::with_name("config")
                                    .short("c")
                                    .long("config")
                                    .value_name("FILE")
                                    .help("Sets a custom config file")
                                    .takes_value(true))
                        .arg(Arg::with_name("limit")
                                    .short("L")
                                    .long("limit")
                                    .value_name("MAX")
                                    .help("Limit to MAX number of repositories scanned")
                                    .takes_value(true))
                        .arg(Arg::with_name("debug")
                                    .short("d")
                                    .multiple(true)
                                    .help("Turn debugging information on"))
                        .get_matches();

    if let Some(c) = matches.value_of("config") {
        println!("Value for config: {}", c);
    }

    let limit = matches.value_of("limit");
    eprintln!("Value for limit: {:?}", limit);

    // You can see how many times a particular flag or argument occurred
    // Note, only flags can have multiple occurrences
    match matches.occurrences_of("debug") {
        0 => println!("Debug mode is off"),
        1 => println!("Debug mode is kind of on"),
        2 => println!("Debug mode is on"),
        3 | _ => println!("Don't be crazy"),
    }

    extern crate config;

    let mut settings = config::Config::default();
    settings
        // Add in `./nebula.toml`
        .merge(config::File::with_name("nebula")).expect("Couldn't get config file")
        // Add in settings from the environment (with a prefix of NEBULA)
        // Eg.. `NEBULA_DEBUG=1 ./target/app` would set the `debug` key
        .merge(config::Environment::with_prefix("NEBULA")).expect("Couldn't get environment variables");

    use crate::node::Node;

    let node = Arc::new(Mutex::new(Node::new(settings, limit)));
    eprintln!("Node setup time: {:?}", node.lock().unwrap().setup_time);

    use nickel::{Nickel, HttpRouter};

    let mut server = Nickel::new();
    let mut router = Nickel::router();

    // the let statements before each route seem excessive
    // though it's the only way I can make the borrow checker happy at the moment
    let anode = Arc::clone(&node);
    router.get("/", middleware!{ |_req, res|
      crate::node::page_template("Nebula Home", anode.lock().unwrap().index_page())
    });
    let anode = Arc::clone(&node);
    router.get("/config", middleware!{ |_req, res|
      crate::node::page_template("Configuration", anode.lock().unwrap().config_page())
    });
    let anode = Arc::clone(&node);
    router.get("/repos", middleware!{ |_req, res|
      crate::node::page_template("Repositories", anode.lock().unwrap().repos_page(None))
    });
    let anode = Arc::clone(&node);
    router.get("/repos/:root", middleware!{ |req, res|
      crate::node::page_template("Repositories", anode.lock().unwrap().repos_page(req.param("root")))
    });
    let anode = Arc::clone(&node);
    router.get("/repos_by_root", middleware!{ |_req, res|
      crate::node::page_template("Repositories by Root Commit", anode.lock().unwrap().repos_root_page())
    });
    // serve Bulma CSS sytlesheet
    router.get("/main.css", middleware!{ |_req, mut res|
      res.set(MediaType::Css);
      BULMA
    });

    server.utilize(router);
    server.listen("127.0.0.1:6767").unwrap();
}
