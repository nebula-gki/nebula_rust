use std::fs::DirEntry;
use git2::{Repository, Time};
use std::path::PathBuf;
use regex::Regex;
use std::io;
use std::time::{Duration, Instant};
use chrono::Local;
use chrono::offset::TimeZone;

// Custom error code based on [Custom Error Types | Learning Rust](https://learning-rust.github.io/docs/e7.custom_error_types.html)
#[derive(Debug)]
pub struct AppError {
    kind: String,    // type of the error
    message: String, // error message
}

impl AppError {
  pub fn new(message: String) -> AppError {
    AppError {
      kind: String::from("runtime"),
      message
    }
  }
}

// Implement std::convert::From for AppError; from io::Error
impl From<io::Error> for AppError {
    fn from(error: io::Error) -> Self {
        AppError {
            kind: String::from("io"),
            message: error.to_string(),
        }
    }
}

// Implement std::convert::From for AppError; from git2::Error
impl From<git2::Error> for AppError {
    fn from(error: git2::Error) -> Self {
        AppError {
            kind: String::from("git2"),
            message: error.to_string(),
        }
    }
}

// TODO: Implement a custom debug method
// derive was causing an error related to the libgit2 bindings
// #[derive(Debug)]
pub struct Repo {
  path: DirEntry,
  repo_url: String,
  root_commit: String,
  last_commit_id: String,
  last_commit_time: Time,
  setup_time: Duration
}

lazy_static! {
  static ref SSH_TO_HTTP_REPLACER: Regex = Regex::new(r"^git@(?P<host>[\w.-]+):(?P<path>.*)$").unwrap();
}

impl Repo {
  pub fn new(path: DirEntry) -> Result<Repo, AppError> {
    let now = Instant::now();

    // ensure that the path is a directory
    if !path.file_type()?.is_dir() {
      return Err(AppError::new(format!("not a directory: {:?}", path)))
    };

    // try to get a Git binding
    let repo = Repository::open(path.path())?;

    // try to find the repo url first
    let remote = repo.find_remote("origin")?;
    let repo_url = remote.url();

    // try to find the root commit
    let mut walker = repo.revwalk()?;
    walker.set_sorting(git2::Sort::NONE);
    match walker.push_head() {
      Ok(()) => (),
      Err(e) => return Err(AppError::from(e))
    }
    let root_commit = match walker.last() {
      None => return Err(AppError::new("can't find first rev".to_string())),
      Some(rev) => rev?.to_string()
    };

    // get last commit info
    let last_commit = repo.head()?.peel_to_commit()?;
    let last_commit_id = last_commit.id().to_string();
    let last_commit_time = last_commit.time();

    match repo_url {
      None => Err(AppError::new("no origin remote".to_string())),
      Some(repo_url) => Ok(Repo {
        path,
        repo_url: repo_url.to_string(),
        root_commit,
        last_commit_id,
        last_commit_time,
        setup_time: now.elapsed()
      })
    }
  }

  pub fn root_commit(self: &Repo) -> String {
    self.root_commit.clone()
  }

  pub fn name(self: &Repo) -> String {
    self.path.file_name().into_string().unwrap()
  }

  pub fn path(self: &Repo) -> PathBuf {
    self.path.path()
  }

  pub fn file_url(self: &Repo) -> String {
    // taken from previous "Surveyor" incarnation in Ruby
    // :TODO: Don't forget to URI encode the path
    // 'txmt://open/?url=file://' + URI.encode(path.cleanpath.to_s)
    match self.path().to_str() {
      Some(path) => format!("txmt://open/?url=file://{}", path),
      None => String::from("#"),
    }
  }

  pub fn repo_url(self: &Repo) -> Option<&String> {
    Some(&self.repo_url)
  }

  pub fn repo_link<'a>(self: &'a Repo) -> Box<horrorshow::RenderBox + 'a> {
    match self.repo_url() {
      None => box_html! {
        : "not a repo"
      },
      Some(link) => box_html! {
        a(href=Repo::ssh_to_http(&link)) {
          : format!("{}", &link);
        }
      }
    }
  }

  pub fn page_entry<'a>(self: &'a Repo) -> Box<horrorshow::Render + 'a> {
    // construct a datetime from commit epoch
    let last_commit_time = Local.timestamp(self.last_commit_time.seconds(), 0);

    box_html! {
      tr {
        td {
          a(href=self.file_url()) {
            : self.name();
          }
        }
        td : format!("{:?}", self.setup_time);
        td : self.repo_link();
        td {
          a(href=format!("/repos/{}", &self.root_commit)) {
            : &self.root_commit;
          }
        }
        td {
          p : self.last_commit_id.as_str();
          p : last_commit_time.to_string();
        }
        td : self.path().to_str();
      }
    }
  }

  // Convert SSH links to GitHub, GitLab, etc to HTTPS links so they are clickable
  pub fn ssh_to_http(url: &str) -> String {
    let result = SSH_TO_HTTP_REPLACER.replace(url, "https://$host/$path");
    // :TODO: return Cow directly?
    result.to_string()
  }
}
