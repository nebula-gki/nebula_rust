use config::Config;

use horrorshow::helper::doctype;

use crate::repo::Repo;

use std::fs;
use std::path::Path;
use std::time::{Duration, Instant};

use std::str::FromStr;

use std::collections::HashMap;
use std::sync::Arc;

pub struct Node {
  config: Config,
  repos: Vec<Arc<Repo>>,
  pub setup_time: Duration,
  repos_by_root: HashMap<String, Vec<Arc<Repo>>>
}

impl Node {
  pub fn new(config: Config, limit: Option<&str>) -> Node {
    let now = Instant::now();
    let repo_paths = config.get_array("repos").expect("Couldn't get repo paths");

    let mut repos: Vec<Arc<Repo>> = Vec::new();
    let mut repos_by_root = HashMap::new();

    for dir in repo_paths {
      if let Ok(dir_path) = dir.into_str() {
        let path = Path::new(&dir_path);

        if let Ok(entries) = fs::read_dir(path) {
          let entry_coll: Vec<_> = entries.collect();
          let lim = match limit {
            Some(lim) => usize::from_str(lim).expect("Illegal limit set") - repos.len(),
            None => entry_coll.len()
          };
          // TODO: make the limit apply to ALL repo_paths collectively, not each separately as now
          // TODO: limit on valid repos, not just by how far we read into a directory
          for dir_entry in entry_coll.into_iter().take(lim) {
            if let Ok(entry) = dir_entry {
              match Repo::new(entry) {
                Err(_) => (),
                Ok(repo) => {
                  let repo = Arc::new(repo);
                  eprintln!("Loaded repo {}: {:?}", repos.len(), repo.path());
                  repos.push(repo.clone());
                  repos_by_root.entry(repo.root_commit()).or_insert(Vec::new()).push(repo.clone());
                }
              }
            }
          }
        }
      }
    }

    Node {
      config,
      repos,
      setup_time: now.elapsed(),
      repos_by_root
    }
  }

  pub fn repos(self: &Node) -> &Vec<Arc<Repo>> {
    &self.repos
  }

  pub fn repos_by_root(self: &Node, commit: &str) -> Option<&Vec<Arc<Repo>>> {
    self.repos_by_root.get(commit)
  }

  pub fn config_page(self: &Node) -> Box<horrorshow::Render> {
    let config_dump = format!("{:#?}", self.config);
    box_html! {
      pre : &config_dump;
    }
  }

  pub fn repos_page(self: &Node, root_commit: Option<&str>) -> Box<horrorshow::Render> {
    let repos = match root_commit {
      None => self.repos(),
      Some(root) => match self.repos_by_root(root) {
        None => self.repos(),
        Some(repo_vec) => repo_vec
      }
    };
    let repo_rows : Vec<Box<horrorshow::Render>> = repos.into_iter().map(|r| r.page_entry()).collect();
    // pre-rendering the rows because we were getting weird lifetime issues when
    // we passed them directly into the template below
    let repo_rows_str = format!("{}", html!{
      @ for repo in &repo_rows {
        : repo;
      }
    });
    box_html! {
      table {
        tr {
          th : "Name";
          th : "Setup Time";
          th : "Origin";
          th : "Root Commit";
          th : "Latest Commit";
          th : "Path";
        }
        : horrorshow::Raw(&repo_rows_str);
      }
    }
  }

  pub fn repos_root_page(self: &Node) -> Box<horrorshow::Render> {
    let rows = format!("{}", html!{
      @ for (commit, repo_list) in self.repos_by_root.iter().filter(|(_c,v)| v.len() > 1) {
        tr {
          td {
            strong : repo_list.len();
          }
          td(colspan="2");
          td(colspan="2") {
            strong : commit;
          }
        }
        @ for repo in repo_list {
          tr {
            : repo.page_entry();
          }
        }
      }
    });
    box_html! {
      table {
        tr {
          th : "Name";
          th : "Setup Time";
          th : "Origin";
          th : "Root Commit";
          th : "Path";
        }
        : horrorshow::Raw(&rows);
      }
    }
  }

  pub fn index_page(self: &Node) -> Box<horrorshow::Render> {
    let setup_time = format!("Setup time: {:?}", self.setup_time);
    box_html!{
      ul {
        li {
          a(href="/config") : "Config"
        }
        li {
          a(href="/repos") : "Repositories"
        }
        li {
          a(href="/repos_by_root") : "Repositories Sharing Root Commits"
        }
      }
      p {
        : &setup_time;
      }
    }
  }
}

pub fn page_template(title: &str, content: Box<horrorshow::Render>) -> String {
  format!("{}", html! {
    head {
      meta(charset="utf-8");
      meta(name="viewport", content="width=device-width, initial-scale=1");
      link(rel="stylesheet", href="/main.css");
      title : &title;
    }
    body {
      section(class="section") {
        div(class="container") {
          h1(id="heading", class="title") : &title;
          : &content;
        }
      }
    }
  })
}
