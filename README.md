# Nebula

A system to organize and manage data.

This is the second [Rust](https://www.rust-lang.org/) implementation of Nebula.

## Status

Currently this is early Alpha code, mostly incomplete and just serving to learn about the Rust programming language.

As this project matures it will provide a centralized, local interface to a various knowledge sources e.g. Git repositories, bookmarks, email, etc.

Eventually it will offer data cross-referencing and de-duplication between these sources.

## Todo

* [ ] Show duplicate repos by
  * [x] root commit
  * [ ] origin url
  * [ ] origin host
* [ ] Cache repo information so everything doesn't have to be re-scanned every time
* [ ] Find readme files
* [ ] Render readme files
* [ ] Render other markdown files
* [ ] Evaluate graph vs. relational database solutions
  * [ ] [IndraDB: A graph database written in rust](https://github.com/indradb/indradb)
* [ ] Evaluate full text search indexing solutions
  * [ ] [Tantivy is a full-text search engine library inspired by Apache Lucene and written in Rust](https://github.com/tantivy-search/tantivy) ([example: simple_search.rs](https://fulmicoton.com/tantivy-examples/simple_search.html))
  * [ ] [Groonga - An open-source fulltext search engine and column store](http://groonga.org/) ([Rust language binding for Groonga.](https://github.com/cosmo0920/ruroonga))
* [ ] Index markdown files with a search solution
* [ ] Allow keyword search by
  * [ ] root commit
  * [ ] words from markdown files
  * [ ] words from text files
  * [ ] words from PDF files
  * [ ] repository remote host names
  * [ ] repository path components
* [ ] Add graph database for ontology building
* [ ] Add proxy support
  * [ ] Intercept web requests as proxy
  * [ ] If a local copy is available, serve that, falling back to remote if necessary
  * [ ] Automatically retain all proxied content for a month or two
  * [ ] Allow proxied content to be permanently archived
